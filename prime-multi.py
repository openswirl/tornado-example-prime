#!/usr/bin/python3
# -*- coding: UTF-8 -*- vim: et ts=4 sw=4

import sys
import os
import time
import signal
import logging

from collections import deque
from concurrent.futures import ProcessPoolExecutor

from tornado import websocket, ioloop, web, gen

from server.prime import ClientHtml, PrimeNonBlocking, Processor
from server.multi import process_loop, sender

_MIN_PYTHON = (3, 5)
if sys.version_info < _MIN_PYTHON:
    sys.exit("Required: Python %s.%s or later.\n" % _MIN_PYTHON)


_HOST = '127.0.0.1'
_PORT = 9669
_WS_PING_INTERVAL_SEC = 5
_WS_PING_TIMEOUT_SEC = 12
_WS_MAX_SIZE_BYTE = 128000

_PROCESSOR_LOOP_MS = 500
_PROCESSOR_WORKER = 5

_SENDER_LOOP_MS = 500

_SHUTDOWN_WAIT_SEC = 5

_PID = os.getpid()


def logging_load():
    logger.setLevel(logging.DEBUG)
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    console.setFormatter(logging.Formatter(
        '%(asctime)s [%(process)d] [%(levelname)s] [%(name)s] - %(message)s'))
    logger.addHandler(console)

async def shutdown(sig_kwargs):
    logger.warning('Shutting down')

    try:
        connected = sig_kwargs.pop('connected')
        processor_loop = sig_kwargs.pop('processor_loop')
        sender_loop = sig_kwargs.pop('sender_loop')
        executor = sig_kwargs.pop('executor')
    except KeyError:
        logger.warning(
            'Shutdown has been called before by another signal. Ignoring')
        return
    except Exception as e:
        logger.exception('Signal Error')
        sys.exit(1)

    ioloop.IOLoop.current().add_callback_from_signal(
        lambda: processor_loop.stop())
    ioloop.IOLoop.current().add_callback_from_signal(
        lambda: executor.shutdown())
    ioloop.IOLoop.current().add_callback_from_signal(
        lambda: sender_loop.stop())

    while connected:
        conn = connected.popitem()
        conn[1].close(code=1000, reason="Server Shutdown")
    await gen.sleep(_SHUTDOWN_WAIT_SEC)
    logger.warning('Stopping IOLoop')
    ioloop.IOLoop.current().stop()
    sys.exit(0)

def signal_trapper(sig_kwargs, signum, frame):
    if os.getpid() != _PID:
        return

    logger.warning('Caught signal: %s ', signum)
    if signum == 15:
        ioloop.IOLoop.current().add_callback_from_signal(shutdown, sig_kwargs)
    elif signum == 1:
        logger.warning("SIGHUP. Do nothing, continue")
    elif signum == 2:
        logger.warning("SIGINT. Using shutdown.")
        ioloop.IOLoop.current().add_callback_from_signal(shutdown, sig_kwargs)
    else:
        pass

def main():
    logging_load()

    try:
        connected = {}
        processes = deque([])
        messages = deque([])

        executor = ProcessPoolExecutor(max_workers=_PROCESSOR_WORKER)
        processor_loop = ioloop.PeriodicCallback(
            lambda: process_loop(executor, connected, messages, processes),
            _PROCESSOR_LOOP_MS)
        processor_loop.start()

        sender_loop = ioloop.PeriodicCallback(
            lambda: sender(connected, messages), _SENDER_LOOP_MS)
        sender_loop.start()

        conn_kwargs = { 
                'connected': connected,
                'processes': processes
            }
        application = web.Application(
            [(r"/", ClientHtml), (r"/ws", PrimeNonBlocking, conn_kwargs),],
            websocket_ping_interval=_WS_PING_INTERVAL_SEC,
            websocket_ping_timeout=_WS_PING_TIMEOUT_SEC,
            websocket_max_message_size=_WS_MAX_SIZE_BYTE
            )
        application.listen(
            _PORT, 
            address=_HOST)
        logger.info("Starting Websocket Server")

        sig_kwargs = {
                'connected': connected,
                'processor_loop': processor_loop,
                'sender_loop': sender_loop,
                'executor': executor
            }

        signal.signal(
            signal.SIGTERM, 
            lambda signum, frame: signal_trapper(sig_kwargs, signum, frame))
        signal.signal(
            signal.SIGHUP, 
            lambda signum, frame: signal_trapper(sig_kwargs, signum, frame))
        signal.signal(
            signal.SIGINT,
            lambda signum, frame: signal_trapper(sig_kwargs, signum, frame))

        ioloop.IOLoop.current().start()
    except Exception as e:
        logger.exception("Could not start server: %s ", str(e))
        sys.exit(1)


if __name__ == "__main__":
    logger = logging.getLogger()
    main()


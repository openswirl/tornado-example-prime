# -*- coding: UTF-8 -*- vim: et ts=4 sw=4

import logging
import math


_CTR_LOG = 10000000

logger = logging.getLogger(__name__)

def process(queue):
    res = None
    try:
        n = int(queue['message'])
        if n < 2:
            raise ValueError
        if n == 2:
            res = "Prime"
        elif n % 2 == 0:
            res = "Odd number"
        else:
            sqrt_n = int(math.floor(math.sqrt(n)))
            counter = 0
            for i in range(3, sqrt_n + 1, 2):
                if n % i == 0:
                    res = "Not a prime"
                    break
                counter += 1
                if counter % _CTR_LOG == 0:
                    logger.debug(
                        "%s - Still processing, loop: %d",
                        str(queue['uuid'])[-12:], counter)
        if res is None:
            res = "Prime"
    except ValueError:
        res = "Try another number"
    except Exception as e:
        res = "Server error"
        logger.exception("Unknown error")
    finally:
        return res

def process_loop(executor, connected, messages, processes=None):
    def send_msg(queue, connected, future):
        try:
            res = future.result()
            payload = str(queue['message']) + " - " + str(res)
            messages.append({
                    'uuid': queue['uuid'],
                    'payload': payload
                })
            logger.debug(
                "%s - Message queued: %s", str(queue['uuid'])[-12:], payload)
        except Exception as e:
            logger.exception("%s - Server Error", str(msg['uuid'])[-12:])

    while processes:
        queue = processes.popleft()
        try:
            fut = executor.submit(process, queue)
            fut.add_done_callback(
                lambda future: send_msg(queue, connected, future))
        except Exception as e:
            logger.exception(
                "%s - Server Error", str(msg['uuid'])[-12:])

async def sender(connected, messages):
    try:
        while messages:
            msg = messages.popleft()
            await connected[msg['uuid']].write_message(msg['payload'])
            logger.debug(
                "%s - Message sent: %s", 
                str(msg['uuid'])[-12:], str(msg['payload']))
    except KeyError:
        logger.warning('%s - Connection already lost', str(msg['uuid'])[-12:])
    except Exception as e:
        logger.exception("%s - Server Error", str(msg['uuid'])[-12:])


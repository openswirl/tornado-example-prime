# -*- coding: UTF-8 -*- vim: et ts=4 sw=4

import logging
import time
import uuid
import math
import sys

from datetime import timedelta

import tornado.websocket
from tornado import web, websocket, ioloop, gen

_MIN_PYTHON = (3, 5)
if sys.version_info < _MIN_PYTHON:
    sys.exit("Required: Python %s.%s or later.\n" % _MIN_PYTHON)


class ClientHtml(web.RequestHandler):
    def get(self):
        self.render('html/index.html')


class Prime(websocket.WebSocketHandler):
    def __init__(self, application, request, **kwargs):
        super(Prime, self).__init__(
            application, request, **kwargs)

        self.remote_ip = (
            self.request.headers.get("X-Real-IP") 
            or self.request.remote_ip)
        self.logger = logging.getLogger(__name__)

        self.ping_time = None
        self.uuid = uuid.uuid4()

    def open(self):
        self.logger.info(
            "Websocket opened for %s with UUID %s",
            str(self.remote_ip), str(self.uuid))

        self.ping_time = time.time()
        self.ping(b'\x04\x00')

    async def on_message(self, message):
        self.logger.debug(
            "%s - Message received", str(self.uuid)[-12:])
        res = None
        try:
            n = int(message)
            if n < 2:
                raise ValueError
            if n == 2:
                res = "Prime"
            elif n % 2 == 0:
                res = "Odd number"
            else:
                sqrt_n = int(math.floor(math.sqrt(n)))
                for i in range(3, sqrt_n + 1, 2):
                    if n % i == 0:
                        res = "Not a prime"
                        break
            if res is None:
                res = "Prime"
        except ValueError:
            res = "Try another number"
        except Exception as e:
            res = "Server error"
            self.logger.exception("Unknown error")
        finally:
            try:
                await self.write_message(str(message) + " - " + str(res))
                self.logger.debug("%s - Message sent", str(self.uuid)[-12:])
            except tornado.websocket.WebSocketClosedError:
                self.logger.warning(
                    "%s - Connection already closed", str(self.uuid)[-12:])
            except Exception as e:
                self.logger.exception(
                    "%s - Server Error", str(self.uuid)[-12:])

    def on_close(self):
        self.logger.info(
            "%s - Websocket closed for %s with code %s and reason %s",
            str(self.uuid)[-12:], str(self.remote_ip), 
            str(self.close_code), str(self.close_reason))
            
    def check_origin(self, origin):
        return True

    def on_pong(self, data):
        if data == b'\x04\x00':
            self.logger.info(
                "%s - Round-Trip Time to %s is %s seconds.", 
                str(self.uuid)[-12:], self.remote_ip, 
                timedelta(seconds=(time.time() - self.ping_time))
                .total_seconds())
        else:
            self.logger.debug("%s - Ping Pong", str(self.uuid)[-12:])


class PrimeNonBlocking(Prime):
    def __init__(self, application, request, **kwargs):
        self.connected = kwargs.pop('connected')
        self.processes = kwargs.pop('processes')

        super(PrimeNonBlocking, self).__init__(
            application, request, **kwargs)

        self.connected[self.uuid] = self

    def on_message(self, message):
        self.processes.append({ 
                'uuid': self.uuid, 
                'message': message
            })
        self.logger.info(
            "%s - Message received and queued", str(self.uuid)[-12:])

    def on_close(self):
        if self.uuid in self.connected:
            del self.connected[self.uuid]
        self.logger.info(
            "%s - Websocket closed for %s with code %s and reason %s",
            str(self.uuid)[-12:], str(self.remote_ip),
            str(self.close_code), str(self.close_reason))


class PrimeMulti(Prime):
    def __init__(self, application, request, **kwargs):
        self.connected = kwargs.pop('connected')
        self.processor = kwargs.pop('processor')

        super(PrimeNonBlocking, self).__init__(
            application, request, **kwargs)

        self.connected[self.uuid] = self

    def on_message(self, message):
        self.processes.append({
                'uuid': self.uuid,
                'message': message
            })
        self.logger.info(
            "%s - Message received and queued", str(self.uuid)[-12:])

    def on_close(self):
        if self.uuid in self.connected:
            del self.connected[self.uuid]
        self.logger.info(
            "%s - Websocket closed for %s with code %s and reason %s",
            str(self.uuid)[-12:], str(self.remote_ip),
            str(self.close_code), str(self.close_reason))


class Processor():
    _CTR_WAITING = 1000000
    _CTR_LOG = 5000000
    _CTR_WAITING_SEC = 0.01

    def __init__(self, connected, processes):
        self.connected = connected
        self.processes = processes
        self.logger = logging.getLogger(__name__)

    async def processor(self):
        while self.processes:
            queue = self.processes.popleft()
            self.logger.debug("%s - Processing", str(queue['uuid'])[-12:])
            res = None
            try:
                n = int(queue['message'])
                if n < 2:
                    raise ValueError
                if n == 2:
                    res = "Prime"
                elif n % 2 == 0:
                    res = "Odd number"
                else:
                    sqrt_n = int(math.floor(math.sqrt(n)))
                    counter = 0
                    for i in range(3, sqrt_n + 1, 2):
                        if n % i == 0:
                            res = "Not a prime"
                            break
                        counter += 1
                        if counter % self._CTR_WAITING == 0:
                            await gen.sleep(self._CTR_WAITING_SEC)
                        if counter % self._CTR_LOG == 0:
                            if queue['uuid'] in self.connected:
                                self.logger.debug(
                                    "%s - Still processing, loop: %d",
                                    str(queue['uuid'])[-12:], counter)
                            else:
                                self.logger.warning(
                                    "%s - Connection already lost",
                                    str(queue['uuid'])[-12:])
                                break
                if res is None:
                    res = "Prime"
            except ValueError:
                res = "Try another number"
            except Exception as e:
                res = "Server error"
                self.logger.exception("Unknown error")
            finally:
                if queue['uuid'] in self.connected:
                    await self.connected[queue['uuid']].write_message(
                        str(queue['message']) + " - " + str(res))
                    self.logger.info(
                        "%s - Message sent", str(queue['uuid'])[-12:])
                else:
                    self.logger.warning(
                        "%s - Connection has been closed",
                        str(queue['uuid'])[-12:])



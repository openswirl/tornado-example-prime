# tornado-example-prime

## Description
An example of Python's Tornado. Application receives a number using websocket 
connection, then calculate is it a prime number or not.

There are 3 main apps:
1.  prime-blocking.py  
    Calculation will be done in `on_message` function.
2.  prime-nonblocking.py  
    `on_message` function will put the number into a queue. A `processor_loop` 
    (using Tornado's `ioloop.PeriodicCallback`) will pull the number from the 
    queue and calculate it. Inside the loop, there are timeouts 
    (`tornado.gen.sleep`) so the process will not block the CPU.
3.  prime-multi.py  
    `processor_loop` will put the number into a process pool (using 
    `concurrent.futures.ProcessPoolExecutor`) to calculate, then put the result 
    into messages queue. Another `sender_loop` is pulling message from the 
    message queue and send it back.
    

## Requirements
- Linux (or any POSIX OS's that can receive signal)
- Python (>=3.5)
- Pip or Pipenv
- Tornado (>=5)
- Modern browser

## Installation and Running

0.  Download
    
    ```bash
    $ git clone 'https://gitlab.com/openswirl/tornado-example-prime.git' $DIR
    ```

1.  Install module  
    With pip  
    
    ```bash
    $ cd $DIR
    $ pip install -r requirements.txt
    ```

    Or, with pipenv  
    
    ```bash
    $ cd $DIR
    $ pipenv --three install
    ```

2.  Run server

    Prime blocking 
    ```bash
    ### Without pipenv
    $ ./prime-blocking.py
    ### With pipenv
    $ pipenv run ./prime-blocking.py
    ```

    Prime non-blocking
    ```bash
    ### Without pipenv
    $ ./prime-nonblocking.py
    ### With pipenv
    $ pipenv run ./prime-nonblocking.py
    ```

    Prime multi-processing
    ```bash
    ### Without pipenv
    $ ./prime-multi.py
    ### Without pipenv
    $ pipenv run ./prime-multi.py
    ```

3.  Shutdown server  
    Use SIGTERM(15) or SIGINT(2)/Ctrl + C

## Testing
1.  Open http://127.0.0.1:9669/ in multiple browser tabs
2.  Input any prime number at the same time before result came.

